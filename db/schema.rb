# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_11_071235) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "cinemas", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "phone"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "city_id"
    t.index ["city_id"], name: "index_cinemas_on_city_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "movies", force: :cascade do |t|
    t.string "title"
    t.string "year"
    t.string "director"
    t.integer "runtime"
    t.string "genre"
    t.text "synopsis"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "rating"
  end

  create_table "orders", force: :cascade do |t|
    t.string "email"
    t.string "credit_card"
    t.bigint "theater_id"
    t.bigint "showtime_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tickets"
    t.bigint "movie_id"
    t.string "name"
    t.string "card_expiration_month"
    t.string "card_expiration_year"
    t.string "street_address"
    t.string "city"
    t.string "state"
    t.string "cvv"
    t.bigint "user_id"
    t.index ["movie_id"], name: "index_orders_on_movie_id"
    t.index ["showtime_id"], name: "index_orders_on_showtime_id"
    t.index ["theater_id"], name: "index_orders_on_theater_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "showtimes", force: :cascade do |t|
    t.datetime "start_time"
    t.bigint "theater_id"
    t.bigint "movie_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["movie_id"], name: "index_showtimes_on_movie_id"
    t.index ["theater_id"], name: "index_showtimes_on_theater_id"
  end

  create_table "theaters", force: :cascade do |t|
    t.string "number"
    t.integer "capacity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cinema_id"
    t.index ["cinema_id"], name: "index_theaters_on_cinema_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "cinemas", "cities"
  add_foreign_key "orders", "movies"
  add_foreign_key "orders", "showtimes"
  add_foreign_key "orders", "theaters"
  add_foreign_key "orders", "users"
  add_foreign_key "showtimes", "movies"
  add_foreign_key "showtimes", "theaters"
  add_foreign_key "theaters", "cinemas"
end
