class DropTableSeat < ActiveRecord::Migration[5.2]
	def up
	    drop_table :seats
	end

	def down
		raise ActiveRecord::IrreversibleMigration
	end
end
