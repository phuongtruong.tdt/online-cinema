class RemoveOrderFromShowtimes < ActiveRecord::Migration[5.2]
  def change
  	remove_reference(:showtimes, :order, index: true)
  end
end
