class RemoveRatingFromMovie < ActiveRecord::Migration[5.2]
  def change
    remove_column :movies, :rating, :string
  end
end
