class CreateCinema < ActiveRecord::Migration[5.2]
  def change
    create_table :cinemas do |t|
    	t.string :name
    	t.string :address
    	t.string :phone
    	t.string :image
      	t.timestamps
    end
  end
end
