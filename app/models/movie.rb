class Movie < ActiveRecord::Base
  has_many :showtimes, dependent: :destroy

  validates :runtime, numericality: { only_integer: true, :greater_than_or_equal_to => 0 }
  validates :rating, numericality: { only_integer: true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 5 }
end
