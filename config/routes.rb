Rails.application.routes.draw do
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users

  root 'movies#index'

  resources :movies, only: [:index] do
    resources :showtimes, only: [:show]
  end

  resources :orders
  resources :cinemas, only: [:index]
  get '/movie/:id/orders' => 'orders#movie_order_review', as: 'movie_order_review'
end